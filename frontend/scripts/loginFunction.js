// JavaScript Document
/*--------------------------
Author: Petr Pěnka
Description: function to control nickname and password
            of user for login
----------------------------
 */


var loginFunction = function (nickname,password) {  
  var xhr = new XMLHttpRequest();
  xhr.open("POST", "http://web-kladno.cz/pinkcrocodile/backend/api/user/auth");
  xhr.setRequestHeader("Content-type", "application/json");
  xhr.onreadystatechange = function () {
    if (this.readyState == 4) {
      if(this.status==200){
        window.location.replace('administration');
      } else{
        alert(JSON.parse(this.responseText));
      }
    }
  }; 
  xhr.send(JSON.stringify({login:nickname, password:password}));
}
