/**
 * Created by Vojtěch Havel
 * Date: 8/3/14
 */
var app = function (response, numOfCat) {
    console.log(response.responseText);

    //set CatagoryTitle - pending - waiting for change of API
    //following code is only temporary
    var CatName = getTranslation('Cat'+numOfCat);
    console.log(CatName);
    $(".CategoryTitle").text(CatName);
    //end of temporary code

    var addWords = function (words) {
        //adds words to document (to WordsList)
        for (var i = 0; i < words.length; i++) {

            var myWord = $("<div>");
            var wordImg = '..' + words[i].img;
            var CatImg = $("<img>");
            CatImg.attr('src', wordImg );
            CatImg.addClass('img-responsive');
            myWord.append(CatImg);
            myWord.addClass("Words");
            myWord.addClass('col-md-3');

            var myLink = "detail/?id=" + words[i].id;
            

            var myHover = $("<div>");
            myHover.addClass('Hover');
            myWord.hover(function(){ $(this).append(myHover);},function(){ $(this).children('.Hover').remove();})

            $(".WordsList").append(myWord);
            myWord.wrap('<a href=' + myLink + '></a>');




        }
        ;

    }


    var wordsObj = JSON.parse(response.responseText);
    //console.log(wordsObj);
    addWords(wordsObj);

}

var main = function () {
    document.title = getTranslation('SummaryTitle');
    //get number of category from url
    var numOfCat = document.URL.split('=')[1];
    //request to list all words in category
    var xhr = new XMLHttpRequest();
    //xhr.open('GET', ApiUrl + '/' + numOfCat + "/words/?" + lang);
    xhr.open('GET', ApiUrl + '/categorywords/' + numOfCat);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (typeof cb !== "undefined") {
                cb(this);
            }
            else {
                app(this, numOfCat);
            }
        }
    };
    xhr.send(null);
}
main();
//
//$(document).ready(main);