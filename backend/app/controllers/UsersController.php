<?php

//Author: Marek Doksanský

class UsersController extends Phalcon\Mvc\Controller {

    public function indexAction() {
        echo $this->view->render('users/index');
    }
  
    private function sendMail($email) {
        /* $to      = $email;
          $subject = 'the subject';
          $message = 'hello';
          $headers = 'From: webmaster@example.com' . "\r\n" .
          'Reply-To: webmaster@example.com' . "\r\n" .
          'X-Mailer: PHP/' . phpversion();
          mail($to, $subject, $message, $headers); */
    }

    public function registerAction() {
      
        $nick = $this->request->getPost('nick', 'string');
        $name = $this->request->getPost('username', 'string');
        $password = $this->request->getPost('password');
        $email = $this->request->getPost('email', 'email');
        $salt =  $this->security->getSaltBytes();
        $role = $this->request->getPost('role');

        $password = crypt($password, $salt);

        $user = new Users();
        $user->nick = $nick;
        $user->name = $name;
        $user->email = $email;
        $user->passwd = $password;
        $user->salt = $salt;
        $user->role = $role;
        


        if ($user->save() == true) {
            //Registration was succussfu
            echo 'Registace probehla uspesne.';
            $this->sendMail($user->email);
        } else {
            echo 'Vyskytl se nasledujici problem: <br>';
            foreach ($user->getMessages() as $message) {
                echo $message->getMessage(), '<br/>';
            }
        }
    }

}
