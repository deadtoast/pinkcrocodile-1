<?php


class Translations extends \Phalcon\Mvc\Model {

	public $id;
	public $cs;
	public $en;
	

	public function get($id = null){
		$words =  $this::find("id=$id");
		foreach($words as $translation){
			$this->id = $translation->id;
			$this->cs = $translation->cs;
			$this->en = $translation->en;		

			return stripslashes($this->toJson());
		}
	}
	public function post($request){
		return $this->save($this->request->getPost(), array('cs','en'));
	}
	public function delete($id){
		$translation = $this::find("id=$id");
		return	$translation->delete();
	}
	
	public function toJson(){
		return json_encode(array('id'=>$this->id,'cs'=>$this->cs,'en'=>$this->en));
	}
}