<?php
/**
 * Created by PhpStorm.
 * User: Dominik
 * Date: 4.8.14
 * Time: 10:01
 */

class Word extends \Phalcon\Mvc\Model {

	public $id;
	public $id_name;
	public $img;
	public $video;
	public $youtube;
	public $sound;
	public $default;
	public $id_creator;
	public $id_category;

	public function get($id = null){
		$words =  $this::find("id=$id");
		foreach($words as $word){
			$this->id = $word->id;
			$this->id_name = $word->id_name;
			$this->img = $word->img;
			$this->video = $word->video;
			$this->youtube = $word->youtube;
			$this->sound = $word->word;
			$this->default = $word->default;
			$this->id_creator = $word->id_creator;
			$this->id_category = $word->id_category;

			return stripslashes($this->toJson());
		}
	}
	public function post($request){
		return $this->save($this->request->getPost(), array('id_name','img','video','youtube','sound','default','id_creator','id_category'));
	}
	public function delete($id){
		$word = $this::find("id=$id");
		return	$word->delete();
	}
	
	public function getSource()
    {
        return 'words';
    }
    
    static function getByCategory($id_category){
		    $word = Words::findFirst(array(
                        "id_category = :id_category:",
                        "bind" => array('id_category' => $id_category)
                    ));         
        return $word;
    }
	
	public function toJson(){
		return json_encode(array('id'=>$this->id,'id_name'=>$this->id_name,'img'=>$this->img,'video'=>$this->video,'youtube'=>$this->youtube,
			'sound'=>$this->sound,'default'=>$this->default,'id_creator'=>$this->id_creator,'id_category'=>$this->id_category));
	}
}